package tailscale

import (
	"bytes"
	"context"
	"log"
	"net/http"
	"os"
)
import "tailscale.com/tsnet"

func Get(path string, url string, hostname string, authorization string, contentType string, tsKey string) string {
	os.Setenv("HOME", path)
	srv := &tsnet.Server{
		Hostname: hostname,
		AuthKey:  tsKey,
	}
	_, err := srv.Up(context.TODO()) // TODO
	if err != nil {
		panic(err)
	}

	defer srv.Close()

	httpCli := srv.HTTPClient()
	// url = "http://tshello"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
		return "blep"
	}

	if authorization != "" {
		req.Header.Set("Authorization", authorization)
	}
	if contentType != "" {
		req.Header.Set("Content-Type", contentType)
	}

	//for header, value := range headers {
	//	req.Header.Add(header, value)
	//}

	resp, err := httpCli.Do(req)
	if err != nil {
		log.Fatal(err)
		return "blep"
	}

	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	return buf.String()
}
