# tailscale

A new Flutter plugin project.

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Run example

To run the example, you need to have a tailscale account and an auth key.
This example will run a GET request on TARGET_URL and show the response on the screen.
```bash
chmod +x build_tailscale_aar.sh
./build_tailscale_aar.sh
cd example
flutter run --dart-define=TAILSCALE_KEY=<tailscale_auth_key> --dart-define=TARGET_URL=http://<host>:<port>
```
