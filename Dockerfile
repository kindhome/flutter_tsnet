FROM mingc/android-build-box:latest
WORKDIR /root
RUN cd && wget https://go.dev/dl/go1.21.4.linux-amd64.tar.gz && tar -xzvf go1.21.4.linux-amd64.tar.gz
RUN apt-get update && apt-get -y upgrade && apt-get install -y netbase
ENV GOROOT_BOOTSTRAP=/root/go
RUN git clone https://github.com/kindhomeio/go.git golang && cd golang/src && git checkout fix_android && ./make.bash
RUN rm -r go && mkdir go
ENV GOPATH=/root/go
ENV PATH="${PATH}:${GOPATH}/bin:/root/golang/bin"
RUN go install golang.org/x/mobile/cmd/gomobile@latest
#RUN go install golang.org/x/mobile/cmd/gobind@latest
RUN gomobile init
COPY go/tailscale.go go/go.mod go/go.sum ./
RUN go get golang.org/x/mobile/cmd/gomobile@latest
#RUN #go get golang.org/x/mobile/bind
RUN gomobile bind -target=android -o tailscale.aar -androidapi 21 kindhome.io/tailscale

# docker cp golang:/root/tailscale.aar example/android/tailscale_bind && docker cp golang:/root/tailscale-sources.jar example/android/tailscale_bind
