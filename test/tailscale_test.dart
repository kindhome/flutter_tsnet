import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_tsnet/tailscale_platform_interface.dart';
import 'package:flutter_tsnet/tailscale_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockTailscalePlatform
    with MockPlatformInterfaceMixin
    implements TailscalePlatform {

  @override
  Future<String?> get(String url, String hostname, Map<String, String>? headers, String? tsKey) {
    // TODO: implement get
    throw UnimplementedError();
  }
}

void main() {
  final TailscalePlatform initialPlatform = TailscalePlatform.instance;

  test('$MethodChannelTailscale is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelTailscale>());
  });
}
