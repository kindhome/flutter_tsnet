#!/usr/bin/bash
docker build -t tailscale_image .
docker run -it -d --name tailscale tailscale_image
docker cp tailscale:/root/tailscale.aar example/android/tailscale_bind && docker cp tailscale:/root/tailscale-sources.jar example/android/tailscale_bind
