import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'tailscale_platform_interface.dart';

/// An implementation of [TailscalePlatform] that uses method channels.
class MethodChannelTailscale extends TailscalePlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('tailscale');

  @override
  Future<String?> get(String url, String hostname, Map<String, String>? headers, String? tsKey) async {
    return await methodChannel.invokeMethod<String>('get', {'url': url, 'hostname': hostname, 'headers': headers, 'tsKey': tsKey});
  }
}
