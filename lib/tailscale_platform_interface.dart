import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'tailscale_method_channel.dart';

abstract class TailscalePlatform extends PlatformInterface {
  /// Constructs a TailscalePlatform.
  TailscalePlatform() : super(token: _token);

  static final Object _token = Object();

  static TailscalePlatform _instance = MethodChannelTailscale();

  /// The default instance of [TailscalePlatform] to use.
  ///
  /// Defaults to [MethodChannelTailscale].
  static TailscalePlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [TailscalePlatform] when
  /// they register themselves.
  static set instance(TailscalePlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> get(String url, String hostname, Map<String, String>? headers, String? tsKey) {
    throw UnimplementedError('get() has not been implemented.');
  }
}
