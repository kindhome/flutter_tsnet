import 'tailscale_platform_interface.dart';

class Tailscale {
  Future<String?> get(String url, String hostname, [Map<String, String>? headers, String? tsKey]) {
    return TailscalePlatform.instance.get(url, hostname, headers, tsKey);
  }
}
