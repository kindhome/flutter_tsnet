package io.kindhome.tailscale

import android.content.Context
import android.content.pm.PackageManager
import tailscale.Tailscale
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result


/** TailscalePlugin */
class TailscalePlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var context: Context

  override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    context = flutterPluginBinding.applicationContext
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "tailscale")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(call: MethodCall, result: Result) {
    if (call.method == "get") {
      val url: String? = call.argument<String>("url")
      val headers: HashMap<String, String>? = call.argument<HashMap<String, String>>("headers")
      // TODO gobind does not support maps
      val authorization = headers?.getOrDefault("Authorization", "")
      val contentType = headers?.getOrDefault("Content-Type", "")
      val tsKey = call.argument<String>("tsKey")
      val hostname = call.argument<String>("hostname")
      result.success(Tailscale.get(context.getApplicationInfo().dataDir, url, hostname, authorization, contentType, tsKey))
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
