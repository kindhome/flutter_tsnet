## 0.0.1

* Initial release.

## 0.0.2

* Update Tailscale get method to include hostname and tsKey parameters
